# Atlassian.Bitbucket.Rest.Client.Refit.Model.Treeentry
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | The path in the repository | [optional] 
**Type** | **string** |  | 
**Commit** | [**Commit**](Commit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

