# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineRefTarget
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Commit** | [**Commit**](Commit.md) |  | [optional] 
**RefType** | **string** | The type of reference (branch/tag). | [optional] 
**Selector** | [**PipelineSelector**](PipelineSelector.md) |  | [optional] 
**RefName** | **string** | The name of the reference. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

