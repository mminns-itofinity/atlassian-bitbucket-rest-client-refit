# Atlassian.Bitbucket.Rest.Client.Refit.Model.Participant
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Role** | **string** |  | [optional] 
**User** | [**User**](User.md) |  | [optional] 
**Approved** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

