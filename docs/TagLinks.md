# Atlassian.Bitbucket.Rest.Client.Refit.Model.TagLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commits** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

