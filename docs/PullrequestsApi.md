# Atlassian.Bitbucket.Rest.Client.Refit.Api.PullrequestsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugDefaultReviewersGet**](PullrequestsApi.md#repositoriesusernamereposlugdefaultreviewersget) | **Get** /repositories/{username}/{repo_slug}/default-reviewers | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete**](PullrequestsApi.md#repositoriesusernamereposlugdefaultreviewerstargetusernamedelete) | **Delete** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet**](PullrequestsApi.md#repositoriesusernamereposlugdefaultreviewerstargetusernameget) | **Get** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut**](PullrequestsApi.md#repositoriesusernamereposlugdefaultreviewerstargetusernameput) | **Put** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugPullrequestsActivityGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestsactivityget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/activity | 
[**RepositoriesUsernameRepoSlugPullrequestsGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestsget) | **Get** /repositories/{username}/{repo_slug}/pullrequests | 
[**RepositoriesUsernameRepoSlugPullrequestsPost**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspost) | **Post** /repositories/{username}/{repo_slug}/pullrequests | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidactivityget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/activity | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidapprovedelete) | **Delete** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/approve | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidapprovepost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/approve | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidcommentscommentidget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments/{comment_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidcommentsget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidcommitsget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/commits | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestiddeclinepost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/decline | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestiddiffget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/diff | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidmergepost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/merge | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidpatchget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/patch | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidput) | **Put** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**](PullrequestsApi.md#repositoriesusernamereposlugpullrequestspullrequestidstatusesget) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses | 


<a name="repositoriesusernamereposlugdefaultreviewersget"></a>
# **RepositoriesUsernameRepoSlugDefaultReviewersGet**
> void RepositoriesUsernameRepoSlugDefaultReviewersGet (string username, string repo_slug)



Returns the repository's default reviewers.  These are the users that are automatically added as reviewers on every new pull request that is created.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugDefaultReviewersGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugDefaultReviewersGet(username, repo_slug);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugDefaultReviewersGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugdefaultreviewerstargetusernamedelete"></a>
# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete**
> Error RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete (string username, string target_username, string repo_slug)



Removes a default reviewer from the repository.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var target_username = target_username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete(username, target_username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **target_username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugdefaultreviewerstargetusernameget"></a>
# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet**
> Error RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet (string username, string target_username, string repo_slug)



Returns the specified reviewer.  This can be used to test whether a user is among the repository's default reviewers list. A 404 indicates that that specified user is not a default reviewer.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var target_username = target_username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet(username, target_username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **target_username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugdefaultreviewerstargetusernameput"></a>
# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut**
> Error RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut (string username, string target_username, string repo_slug)



Adds the specified user to the repository's list of default reviewers.  This method is idempotent. Adding a user a second time has no effect.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePutExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var target_username = target_username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut(username, target_username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **target_username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestsactivityget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsActivityGet**
> void RepositoriesUsernameRepoSlugPullrequestsActivityGet (string username, string repo_slug, int? pull_request_id)



Returns a paginated list of the pull request's activity log.  This includes comments that were made by the reviewers, updates and approvals.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsActivityGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the user, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var pull_request_id = 56;  // int? | The id of the pull request. 

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugPullrequestsActivityGet(username, repo_slug, pull_request_id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsActivityGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the user, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **pull_request_id** | **int?**| The id of the pull request.  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestsget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsGet**
> PaginatedPullrequests RepositoriesUsernameRepoSlugPullrequestsGet (string username, string repo_slug, string state = null)



Returns a paginated list of all pull requests on the specified repository. By default only open pull requests are returned. This can be controlled using the `state` query parameter. To retrieve pull requests that are in one of multiple states, repeat the `state` parameter for each individual state.  This endpoint also supports filtering and sorting of the results. See [filtering and sorting](../../../../meta/filtering) for more details.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the user, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var state = state_example;  // string | Only return pull requests that are in this state. This parameter can be repeated. (optional) 

            try
            {
                PaginatedPullrequests result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsGet(username, repo_slug, state);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the user, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **state** | **string**| Only return pull requests that are in this state. This parameter can be repeated. | [optional] 

### Return type

[**PaginatedPullrequests**](PaginatedPullrequests.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspost"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPost (string username, string repo_slug, Pullrequest _body = null)



Creates a new pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the user, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var _body = new Pullrequest(); // Pullrequest | The new pull request.  The request URL you POST to becomes the destination repository URL. For this reason, you must specify an explicit source repository in the request object if you want to pull from a different repository (fork).  Since not all elements are required or even mutable, you only need to include the elements you want to initialize, such as the source branch and the title. (optional) 

            try
            {
                Pullrequest result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPost(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the user, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **_body** | [**Pullrequest**](Pullrequest.md)| The new pull request.  The request URL you POST to becomes the destination repository URL. For this reason, you must specify an explicit source repository in the request object if you want to pull from a different repository (fork).  Since not all elements are required or even mutable, you only need to include the elements you want to initialize, such as the source branch and the title. | [optional] 

### Return type

[**Pullrequest**](Pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidactivityget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet**
> void RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet (string username, string repo_slug, int? pull_request_id)



Returns a paginated list of the pull request's activity log.  This includes comments that were made by the reviewers, updates and approvals.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the user, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var pull_request_id = 56;  // int? | The id of the pull request. 

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet(username, repo_slug, pull_request_id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the user, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **pull_request_id** | **int?**| The id of the pull request.  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidapprovedelete"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete (string username, string pull_request_id, string repo_slug)



Redact the authenticated user's approval of the specified pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidapprovepost"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost (string username, string pull_request_id, string repo_slug)



Approve the specified pull request as the authenticated user.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidcommentscommentidget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet (string username, string pull_request_id, string comment_id, string repo_slug)



Returns a specific pull request comment.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var comment_id = comment_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet(username, pull_request_id, comment_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **comment_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidcommentsget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet (string username, string pull_request_id, string repo_slug)



Returns a paginated list of the pull request's comments.  This includes both global, inline comments and replies.  The default sorting is oldest to newest and can be overridden with the `sort` query parameter.  This endpoint also supports filtering and sorting of the results. See [filtering and sorting](../../../../../../meta/filtering) for more details.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidcommitsget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet (string username, string pull_request_id, string repo_slug)



Returns a paginated list of the pull request's commits.  These are the commits that are being merged into the destination branch when the pull requests gets accepted.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestiddeclinepost"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost (string username, string pull_request_id, string repo_slug)



Declines the pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Pullrequest result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Pullrequest**](Pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestiddiffget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet (string username, string pull_request_id, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet (string username, string repo_slug, int? pull_request_id)



Returns the specified pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the account, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var pull_request_id = 56;  // int? | The id of the pull request. 

            try
            {
                Pullrequest result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet(username, repo_slug, pull_request_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the account, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **pull_request_id** | **int?**| The id of the pull request.  | 

### Return type

[**Pullrequest**](Pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidmergepost"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost (string username, string pull_request_id, string repo_slug, PullrequestMergeParameters _body = null)



Merges the pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var _body = new PullrequestMergeParameters(); // PullrequestMergeParameters |  (optional) 

            try
            {
                Pullrequest result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost(username, pull_request_id, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 
 **_body** | [**PullrequestMergeParameters**](PullrequestMergeParameters.md)|  | [optional] 

### Return type

[**Pullrequest**](Pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidpatchget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet**
> Error RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet (string username, string pull_request_id, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var pull_request_id = pull_request_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet(username, pull_request_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pull_request_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidput"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut (string username, string repo_slug, int? pull_request_id, Pullrequest _body = null)



Mutates the specified pull request.  This can be used to change the pull request's branches or description.  Only open pull requests can be mutated.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPutExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | This can either be the username or the UUID of the user, surrounded by curly-braces, for example: `{user UUID}`. 
            var repo_slug = repo_slug_example;  // string | This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: `{repository UUID}`. 
            var pull_request_id = 56;  // int? | The id of the open pull request. 
            var _body = new Pullrequest(); // Pullrequest | The pull request that is to be updated. (optional) 

            try
            {
                Pullrequest result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut(username, repo_slug, pull_request_id, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| This can either be the username or the UUID of the user, surrounded by curly-braces, for example: &#x60;{user UUID}&#x60;.  | 
 **repo_slug** | **string**| This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: &#x60;{repository UUID}&#x60;.  | 
 **pull_request_id** | **int?**| The id of the open pull request.  | 
 **_body** | [**Pullrequest**](Pullrequest.md)| The pull request that is to be updated. | [optional] 

### Return type

[**Pullrequest**](Pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugpullrequestspullrequestidstatusesget"></a>
# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**
> PaginatedCommitstatuses RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet (string username, string repo_slug, int? pull_request_id)



Returns all statuses (e.g. build results) for the given pull request.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PullrequestsApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var pull_request_id = 56;  // int? | The pull request's id

            try
            {
                PaginatedCommitstatuses result = apiInstance.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet(username, repo_slug, pull_request_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PullrequestsApi.RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **pull_request_id** | **int?**| The pull request&#39;s id | 

### Return type

[**PaginatedCommitstatuses**](PaginatedCommitstatuses.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

