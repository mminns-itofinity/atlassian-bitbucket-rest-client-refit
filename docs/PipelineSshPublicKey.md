# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineSshPublicKey
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**KeyType** | **string** | The type of the public key. | [optional] 
**Md5Fingerprint** | **string** | The MD5 fingerprint of the public key. | [optional] 
**Sha256Fingerprint** | **string** | The SHA-256 fingerprint of the public key. | [optional] 
**Key** | **string** | The base64 encoded public key. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

