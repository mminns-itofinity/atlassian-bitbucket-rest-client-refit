# Atlassian.Bitbucket.Rest.Client.Refit.Model.SnippetComment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Snippet** | [**Snippet**](Snippet.md) |  | [optional] 
**Links** | [**GroupLinks**](GroupLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

