# Atlassian.Bitbucket.Rest.Client.Refit.Model.Pipeline
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Uuid** | **string** | The UUID identifying the pipeline. | [optional] 
**Repository** | [**Repository**](Repository.md) |  | [optional] 
**Creator** | [**Account**](Account.md) | The Bitbucket account that was used to create the pipeline. | [optional] 
**State** | [**PipelineState**](PipelineState.md) |  | [optional] 
**CreatedOn** | **DateTime?** | The timestamp when the pipeline was created. | [optional] 
**Trigger** | [**PipelineTrigger**](PipelineTrigger.md) | The trigger used for the pipeline. | [optional] 
**BuildSecondsUsed** | **int?** | The number of build seconds used by this pipeline. | [optional] 
**CompletedOn** | **DateTime?** | The timestamp when the Pipeline was completed. This is not set if the pipeline is still in progress. | [optional] 
**Target** | [**PipelineTarget**](PipelineTarget.md) | The target that the pipeline built. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

