# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineLogRange
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LastBytePosition** | **int?** | The position of the last byte of the range in the log. | [optional] 
**FirstBytePosition** | **int?** | The position of the first byte of the range in the log. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

