# Atlassian.Bitbucket.Rest.Client.Refit.Model.CommitLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Comments** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Patch** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Diff** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Approve** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Statuses** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

