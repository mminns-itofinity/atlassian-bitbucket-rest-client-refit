# Atlassian.Bitbucket.Rest.Client.Refit.Model.User
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Username** | **string** |  | [optional] 
**Website** | **string** |  | [optional] 
**DisplayName** | **string** |  | [optional] 
**Uuid** | **string** |  | [optional] 
**Links** | [**AccountLinks**](AccountLinks.md) |  | [optional] 
**CreatedOn** | **DateTime?** |  | [optional] 
**IsStaff** | **bool?** |  | [optional] 
**AccountId** | **string** | The user&#39;s Atlassian account ID. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

