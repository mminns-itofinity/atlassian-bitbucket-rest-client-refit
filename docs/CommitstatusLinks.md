# Atlassian.Bitbucket.Rest.Client.Refit.Model.CommitstatusLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commit** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

