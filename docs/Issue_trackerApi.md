# Atlassian.Bitbucket.Rest.Client.Refit.Api.Issue_trackerApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugComponentsComponentIdGet**](Issue_trackerApi.md#repositoriesusernamereposlugcomponentscomponentidget) | **Get** /repositories/{username}/{repo_slug}/components/{component_id} | 
[**RepositoriesUsernameRepoSlugComponentsGet**](Issue_trackerApi.md#repositoriesusernamereposlugcomponentsget) | **Get** /repositories/{username}/{repo_slug}/components | 
[**RepositoriesUsernameRepoSlugIssuesGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesget) | **Get** /repositories/{username}/{repo_slug}/issues | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidattachmentsget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidattachmentspathdelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidattachmentspathget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidattachmentspost) | **Post** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidcommentscommentidget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/comments/{comment_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidcommentsget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/comments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdDelete**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueiddelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidvotedelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidvoteget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVotePut**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidvoteput) | **Put** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidwatchdelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidwatchget) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut**](Issue_trackerApi.md#repositoriesusernamereposlugissuesissueidwatchput) | **Put** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesPost**](Issue_trackerApi.md#repositoriesusernamereposlugissuespost) | **Post** /repositories/{username}/{repo_slug}/issues | 
[**RepositoriesUsernameRepoSlugMilestonesGet**](Issue_trackerApi.md#repositoriesusernamereposlugmilestonesget) | **Get** /repositories/{username}/{repo_slug}/milestones | 
[**RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet**](Issue_trackerApi.md#repositoriesusernamereposlugmilestonesmilestoneidget) | **Get** /repositories/{username}/{repo_slug}/milestones/{milestone_id} | 
[**RepositoriesUsernameRepoSlugVersionsGet**](Issue_trackerApi.md#repositoriesusernamereposlugversionsget) | **Get** /repositories/{username}/{repo_slug}/versions | 
[**RepositoriesUsernameRepoSlugVersionsVersionIdGet**](Issue_trackerApi.md#repositoriesusernamereposlugversionsversionidget) | **Get** /repositories/{username}/{repo_slug}/versions/{version_id} | 


<a name="repositoriesusernamereposlugcomponentscomponentidget"></a>
# **RepositoriesUsernameRepoSlugComponentsComponentIdGet**
> Component RepositoriesUsernameRepoSlugComponentsComponentIdGet (string username, string repo_slug, int? component_id)



Returns the specified issue tracker component object.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugComponentsComponentIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var component_id = 56;  // int? | The component's id

            try
            {
                Component result = apiInstance.RepositoriesUsernameRepoSlugComponentsComponentIdGet(username, repo_slug, component_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugComponentsComponentIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **component_id** | **int?**| The component&#39;s id | 

### Return type

[**Component**](Component.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugcomponentsget"></a>
# **RepositoriesUsernameRepoSlugComponentsGet**
> PaginatedComponents RepositoriesUsernameRepoSlugComponentsGet (string username, string repo_slug)



Returns the components that have been defined in the issue tracker.  This resource is only available on repositories that have the issue tracker enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugComponentsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                PaginatedComponents result = apiInstance.RepositoriesUsernameRepoSlugComponentsGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugComponentsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**PaginatedComponents**](PaginatedComponents.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesget"></a>
# **RepositoriesUsernameRepoSlugIssuesGet**
> PaginatedIssues RepositoriesUsernameRepoSlugIssuesGet (string username, string repo_slug)



Returns the issues in the issue tracker.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                PaginatedIssues result = apiInstance.RepositoriesUsernameRepoSlugIssuesGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**PaginatedIssues**](PaginatedIssues.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidattachmentsget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet**
> PaginatedIssueAttachments RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet (string username, string repo_slug, int? issue_id)



Returns all attachments for this issue.  This returns the files' meta data. This does not return the files' actual contents.  The files are always ordered by their upload date.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                PaginatedIssueAttachments result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**PaginatedIssueAttachments**](PaginatedIssueAttachments.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidattachmentspathdelete"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete**
> void RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete (string username, string path, string issue_id, string repo_slug)



Deletes an attachment.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var path = path_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete(username, path, issue_id, repo_slug);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **path** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidattachmentspathget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet**
> void RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet (string username, string path, string issue_id, string repo_slug)



Returns the contents of the specified file attachment.  Note that this endpoint does not return a JSON response, but instead returns a redirect pointing to the actual file that in turn will return the raw contents.  The redirect URL contains a one-time token that has a limited lifetime. As a result, the link should not be persisted, stored, or shared.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var path = path_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet(username, path, issue_id, repo_slug);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **path** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidattachmentspost"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost**
> void RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost (string username, string repo_slug, int? issue_id)



Upload new issue attachments.  To upload files, perform a `multipart/form-data` POST containing one or more file fields.  When a file is uploaded with the same name as an existing attachment, then the existing file will be replaced.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost(username, repo_slug, issue_id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidcommentscommentidget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet (string username, string comment_id, string issue_id, string repo_slug)



Returns the specified issue comment object.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var comment_id = comment_id_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet(username, comment_id, issue_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **comment_id** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidcommentsget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet (string username, string issue_id, string repo_slug)



Returns all comments that were made on the specified issue.  The default sorting is oldest to newest and can be overridden with the `sort` query parameter.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet(username, issue_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueiddelete"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdDelete**
> Issue RepositoriesUsernameRepoSlugIssuesIssueIdDelete (string username, string issue_id, string repo_slug)



Deletes the specified issue. This requires write access to the repository.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Issue result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdDelete(username, issue_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdGet**
> Issue RepositoriesUsernameRepoSlugIssuesIssueIdGet (string username, string issue_id, string repo_slug)



Returns the specified issue.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var issue_id = issue_id_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Issue result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdGet(username, issue_id, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issue_id** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidvotedelete"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete (string username, string repo_slug, int? issue_id)



Retract your vote.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdVoteDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidvoteget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet (string username, string repo_slug, int? issue_id)



Check whether the authenticated user has voted for this issue. A 204 status code indicates that the user has voted, while a 404 implies they haven't.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdVoteGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidvoteput"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdVotePut**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdVotePut (string username, string repo_slug, int? issue_id)



Vote for this issue.  To cast your vote, do an empty PUT. The 204 status code indicates that the operation was successful.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdVotePutExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdVotePut(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdVotePut: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidwatchdelete"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete (string username, string repo_slug, int? issue_id)



Stop watching this issue.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdWatchDeleteExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidwatchget"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet (string username, string repo_slug, int? issue_id)



Indicated whether or not the authenticated user is watching this issue.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdWatchGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuesissueidwatchput"></a>
# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut**
> Error RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut (string username, string repo_slug, int? issue_id)



Start watching this issue.  To start watching this issue, do an empty PUT. The 204 status code indicates that the operation was successful.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesIssueIdWatchPutExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var issue_id = 56;  // int? | The issue's id

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut(username, repo_slug, issue_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **issue_id** | **int?**| The issue&#39;s id | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugissuespost"></a>
# **RepositoriesUsernameRepoSlugIssuesPost**
> Issue RepositoriesUsernameRepoSlugIssuesPost (string username, string repo_slug, Issue _body)



Creates a new issue.  This call requires authentication. Private repositories or private issue trackers require the caller to authenticate with an account that has appropriate authorisation.  The authenticated user is used for the issue's `reporter` field.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugIssuesPostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var _body = new Issue(); // Issue | The new issue. Note that the only required element is `title`. All other elements can be omitted from the body.

            try
            {
                Issue result = apiInstance.RepositoriesUsernameRepoSlugIssuesPost(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugIssuesPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **_body** | [**Issue**](Issue.md)| The new issue. Note that the only required element is &#x60;title&#x60;. All other elements can be omitted from the body. | 

### Return type

[**Issue**](Issue.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugmilestonesget"></a>
# **RepositoriesUsernameRepoSlugMilestonesGet**
> PaginatedMilestones RepositoriesUsernameRepoSlugMilestonesGet (string username, string repo_slug)



Returns the milestones that have been defined in the issue tracker.  This resource is only available on repositories that have the issue tracker enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugMilestonesGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                PaginatedMilestones result = apiInstance.RepositoriesUsernameRepoSlugMilestonesGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugMilestonesGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**PaginatedMilestones**](PaginatedMilestones.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugmilestonesmilestoneidget"></a>
# **RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet**
> Milestone RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet (string username, string repo_slug, int? milestone_id)



Returns the specified issue tracker milestone object.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugMilestonesMilestoneIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var milestone_id = 56;  // int? | The milestone's id

            try
            {
                Milestone result = apiInstance.RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet(username, repo_slug, milestone_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **milestone_id** | **int?**| The milestone&#39;s id | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugversionsget"></a>
# **RepositoriesUsernameRepoSlugVersionsGet**
> PaginatedVersions RepositoriesUsernameRepoSlugVersionsGet (string username, string repo_slug)



Returns the versions that have been defined in the issue tracker.  This resource is only available on repositories that have the issue tracker enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugVersionsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                PaginatedVersions result = apiInstance.RepositoriesUsernameRepoSlugVersionsGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugVersionsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**PaginatedVersions**](PaginatedVersions.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugversionsversionidget"></a>
# **RepositoriesUsernameRepoSlugVersionsVersionIdGet**
> Version RepositoriesUsernameRepoSlugVersionsVersionIdGet (string username, string repo_slug, int? version_id)



Returns the specified issue tracker version object.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugVersionsVersionIdGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new Issue_trackerApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 
            var version_id = 56;  // int? | The version's id

            try
            {
                Version result = apiInstance.RepositoriesUsernameRepoSlugVersionsVersionIdGet(username, repo_slug, version_id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Issue_trackerApi.RepositoriesUsernameRepoSlugVersionsVersionIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 
 **version_id** | **int?**| The version&#39;s id | 

### Return type

[**Version**](Version.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

