# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Message** | **string** | The error message. | [optional] 
**Key** | **string** | The error key. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

