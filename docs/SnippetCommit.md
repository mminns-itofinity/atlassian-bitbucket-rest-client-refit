# Atlassian.Bitbucket.Rest.Client.Refit.Model.SnippetCommit
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Date** | **DateTime?** |  | [optional] 
**Hash** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 
**Parents** | [**List&lt;BaseCommit&gt;**](BaseCommit.md) |  | [optional] 
**Author** | [**Author**](Author.md) |  | [optional] 
**Snippet** | [**Snippet**](Snippet.md) |  | [optional] 
**Links** | [**SnippetCommitLinks**](SnippetCommitLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

