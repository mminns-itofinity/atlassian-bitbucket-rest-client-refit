# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineStepStateCompleted
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Name** | **string** | The name of pipeline step state (COMPLETED). | [optional] 
**Result** | [**PipelineStepStateCompletedResult**](PipelineStepStateCompletedResult.md) | A result of a completed state of a pipeline step. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

