# Atlassian.Bitbucket.Rest.Client.Refit.Model.Tag
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the tag. | [optional] 
**Links** | [**TagLinks**](TagLinks.md) |  | [optional] 
**Tagger** | [**Author**](Author.md) |  | [optional] 
**Date** | **DateTime?** | The date that the tag was created, if available | [optional] 
**Message** | **string** | The message associated with the tag, if available. | [optional] 
**Type** | **string** |  | 
**Target** | [**Commit**](Commit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

