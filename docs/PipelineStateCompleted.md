# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineStateCompleted
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Name** | **string** | The name of pipeline state (COMPLETED). | [optional] 
**Result** | [**PipelineStateCompletedResult**](PipelineStateCompletedResult.md) | A result of a completed state of a pipeline. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

