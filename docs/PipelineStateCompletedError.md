# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineStateCompletedError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Name** | **string** | The name of the result (ERROR) | [optional] 
**Error** | [**PipelineError**](PipelineError.md) | An error result of a completed state of a Bitbucket Pipeline. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

