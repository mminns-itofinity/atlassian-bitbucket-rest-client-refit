# Atlassian.Bitbucket.Rest.Client.Refit.Model.Commit
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Date** | **DateTime?** |  | [optional] 
**Hash** | **string** |  | [optional] 
**Message** | **string** |  | [optional] 
**Parents** | [**List&lt;BaseCommit&gt;**](BaseCommit.md) |  | [optional] 
**Author** | [**Author**](Author.md) |  | [optional] 
**Participants** | [**List&lt;Participant&gt;**](Participant.md) |  | [optional] 
**Links** | [**CommitLinks**](CommitLinks.md) |  | [optional] 
**Repository** | [**Repository**](Repository.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

