# Atlassian.Bitbucket.Rest.Client.Refit.Model.PullrequestLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Decline** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Commits** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Comments** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Merge** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Activity** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Diff** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Approve** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

