# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineVariable
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Value** | **string** | The value of the variable. If the variable is secured, this will be empty. | [optional] 
**Secured** | **bool?** | If true, this variable will be treated as secured. The value will never be exposed in the logs or the REST API. | [optional] 
**Uuid** | **string** | The UUID identifying the variable. | [optional] 
**Key** | **string** | The unique name of the variable. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

