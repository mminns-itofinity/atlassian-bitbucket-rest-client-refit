# Atlassian.Bitbucket.Rest.Client.Refit.Model.ProjectLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Avatar** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

