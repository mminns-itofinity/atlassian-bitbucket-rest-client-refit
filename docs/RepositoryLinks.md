# Atlassian.Bitbucket.Rest.Client.Refit.Model.RepositoryLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Downloads** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Watchers** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Commits** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Clone** | [**List&lt;SubjectTypesUserEvents&gt;**](SubjectTypesUserEvents.md) |  | [optional] 
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Avatar** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Hooks** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Forks** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Pullrequests** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

