# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineScheduleExecutionExecuted
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Pipeline** | [**Pipeline**](Pipeline.md) | The pipeline that was triggered by this execution of a schedule. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

