# Atlassian.Bitbucket.Rest.Client.Refit.Model.Repository
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Scm** | **string** |  | [optional] 
**HasWiki** | **bool?** |  | [optional] 
**Uuid** | **string** | The repository&#39;s immutable id. This can be used as a substitute for the slug segment in URLs. Doing this guarantees your URLs will survive renaming of the repository by its owner, or even transfer of the repository to a different user. | [optional] 
**Links** | [**RepositoryLinks**](RepositoryLinks.md) |  | [optional] 
**ForkPolicy** | **string** |  Controls the rules for forking this repository.  * **allow_forks**: unrestricted forking * **no_public_forks**: restrict forking to private forks (forks cannot   be made public later) * **no_forks**: deny all forking  | [optional] 
**Name** | **string** |  | [optional] 
**Project** | [**Project**](Project.md) |  | [optional] 
**Language** | **string** |  | [optional] 
**CreatedOn** | **DateTime?** |  | [optional] 
**Parent** | [**Repository**](Repository.md) |  | [optional] 
**FullName** | **string** | The concatenation of the repository owner&#39;s username and the slugified name, e.g. \&quot;evzijst/interruptingcow\&quot;. This is the same string used in Bitbucket URLs. | [optional] 
**UpdatedOn** | **DateTime?** |  | [optional] 
**Owner** | [**Account**](Account.md) |  | [optional] 
**HasIssues** | **bool?** |  | [optional] 
**Size** | **int?** |  | [optional] 
**IsPrivate** | **bool?** |  | [optional] 
**Description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

