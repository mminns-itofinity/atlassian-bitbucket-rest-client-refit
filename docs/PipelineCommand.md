# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineCommand
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Command** | **string** | The executable command. | [optional] 
**Name** | **string** | The name of the command. | [optional] 
**LogRange** | [**PipelineLogRange**](PipelineLogRange.md) | The range in the log that contains the execution output of this command. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

