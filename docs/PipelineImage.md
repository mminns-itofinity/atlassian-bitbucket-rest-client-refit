# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineImage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | The username needed to authenticate with the Docker registry. Only required when using a private Docker image. | [optional] 
**Password** | **string** | The password needed to authenticate with the Docker registry. Only required when using a private Docker image. | [optional] 
**Name** | **string** | The name of the image. If the image is hosted on DockerHub the short name can be used, otherwise the fully qualified name is required here. | [optional] 
**Email** | **string** | The email needed to authenticate with the Docker registry. Only required when using a private Docker image. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

