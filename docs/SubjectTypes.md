# Atlassian.Bitbucket.Rest.Client.Refit.Model.SubjectTypes
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**User** | [**SubjectTypesUser**](SubjectTypesUser.md) |  | [optional] 
**Repository** | [**SubjectTypesUser**](SubjectTypesUser.md) |  | [optional] 
**Team** | [**SubjectTypesUser**](SubjectTypesUser.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

