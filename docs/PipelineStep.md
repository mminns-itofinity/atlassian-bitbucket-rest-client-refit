# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineStep
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**CompletedOn** | **DateTime?** | The timestamp when the step execution was completed. This is not set if the step is still in progress. | [optional] 
**LogByteCount** | **int?** | The amount of bytes of the log file that is available. | [optional] 
**Image** | [**PipelineImage**](PipelineImage.md) | The Docker image used as the build container for the step. | [optional] 
**StartedOn** | **DateTime?** | The timestamp when the step execution was started. This is not set when the step hasn&#39;t executed yet. | [optional] 
**ScriptCommands** | [**List&lt;PipelineCommand&gt;**](PipelineCommand.md) | The list of build commands. These commands are executed in the build container. | [optional] 
**State** | [**PipelineStepState**](PipelineStepState.md) | The current state of the step | [optional] 
**SetupCommands** | [**List&lt;PipelineCommand&gt;**](PipelineCommand.md) | The list of commands that are executed as part of the setup phase of the build. These commands are executed outside the build container. | [optional] 
**Uuid** | **string** | The UUID identifying the step. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

