# Atlassian.Bitbucket.Rest.Client.Refit.Model.Snippet
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Scm** | **string** | The DVCS used to store the snippet. | [optional] 
**Creator** | [**Account**](Account.md) |  | [optional] 
**Title** | **string** |  | [optional] 
**CreatedOn** | **DateTime?** |  | [optional] 
**Owner** | [**Account**](Account.md) |  | [optional] 
**UpdatedOn** | **DateTime?** |  | [optional] 
**Id** | **int?** |  | [optional] 
**IsPrivate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

