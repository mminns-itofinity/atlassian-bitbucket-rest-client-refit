# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineSshKeyPair
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**PublicKey** | **string** | The SSH public key. | [optional] 
**PrivateKey** | **string** | The SSH private key. This value will be empty when retrieving the SSH key pair. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

