# Atlassian.Bitbucket.Rest.Client.Refit.Model.Issue
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Priority** | **string** |  | [optional] 
**Kind** | **string** |  | [optional] 
**Repository** | [**Repository**](Repository.md) |  | [optional] 
**Links** | [**IssueLinks**](IssueLinks.md) |  | [optional] 
**Reporter** | [**User**](User.md) |  | [optional] 
**Title** | **string** |  | [optional] 
**Component** | [**Component**](Component.md) |  | [optional] 
**Votes** | **int?** |  | [optional] 
**Content** | [**IssueContent**](IssueContent.md) |  | [optional] 
**Assignee** | [**User**](User.md) |  | [optional] 
**State** | **string** |  | [optional] 
**Version** | [**Version**](Version.md) |  | [optional] 
**EditedOn** | **DateTime?** |  | [optional] 
**CreatedOn** | **DateTime?** |  | [optional] 
**Milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**UpdatedOn** | **DateTime?** |  | [optional] 
**Id** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

