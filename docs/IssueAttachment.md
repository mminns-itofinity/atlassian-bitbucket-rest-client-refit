# Atlassian.Bitbucket.Rest.Client.Refit.Model.IssueAttachment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Name** | **string** |  | [optional] 
**Links** | [**MilestoneLinks**](MilestoneLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

