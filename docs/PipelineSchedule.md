# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineSchedule
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Uuid** | **string** | The UUID identifying the schedule. | [optional] 
**CronPattern** | **string** | The cron expression that the schedule applies. | [optional] 
**Selector** | [**PipelineSelector**](PipelineSelector.md) |  | [optional] 
**CreatedOn** | **DateTime?** | The timestamp when the schedule was created. | [optional] 
**UpdatedOn** | **DateTime?** | The timestamp when the schedule was updated. | [optional] 
**Target** | [**PipelineTarget**](PipelineTarget.md) | The target on which the schedule will be executed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

