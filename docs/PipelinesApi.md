# Atlassian.Bitbucket.Rest.Client.Refit.Api.PipelinesApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreatePipelineForRepository**](PipelinesApi.md#createpipelineforrepository) | **Post** /repositories/{username}/{repo_slug}/pipelines/ | 
[**CreatePipelineVariableForTeam**](PipelinesApi.md#createpipelinevariableforteam) | **Post** /teams/{username}/pipelines_config/variables/ | 
[**CreatePipelineVariableForUser**](PipelinesApi.md#createpipelinevariableforuser) | **Post** /users/{username}/pipelines_config/variables/ | 
[**CreateRepositoryPipelineKnownHost**](PipelinesApi.md#createrepositorypipelineknownhost) | **Post** /repositories/{username}/{repo_slug}/pipelines_config/ssh/known_hosts/ | 
[**CreateRepositoryPipelineSchedule**](PipelinesApi.md#createrepositorypipelineschedule) | **Post** /repositories/{username}/{repo_slug}/pipelines_config/schedules/ | 
[**CreateRepositoryPipelineVariable**](PipelinesApi.md#createrepositorypipelinevariable) | **Post** /repositories/{username}/{repo_slug}/pipelines_config/variables/ | 
[**DeletePipelineVariableForTeam**](PipelinesApi.md#deletepipelinevariableforteam) | **Delete** /teams/{username}/pipelines_config/variables/{variable_uuid} | 
[**DeletePipelineVariableForUser**](PipelinesApi.md#deletepipelinevariableforuser) | **Delete** /users/{username}/pipelines_config/variables/{variable_uuid} | 
[**DeleteRepositoryPipelineKeyPair**](PipelinesApi.md#deleterepositorypipelinekeypair) | **Delete** /repositories/{username}/{repo_slug}/pipelines_config/ssh/key_pair | 
[**DeleteRepositoryPipelineKnownHost**](PipelinesApi.md#deleterepositorypipelineknownhost) | **Delete** /repositories/{username}/{repo_slug}/pipelines_config/ssh/known_hosts/{known_host_uuid} | 
[**DeleteRepositoryPipelineSchedule**](PipelinesApi.md#deleterepositorypipelineschedule) | **Delete** /repositories/{username}/{repo_slug}/pipelines_config/schedules/{schedule_uuid} | 
[**DeleteRepositoryPipelineVariable**](PipelinesApi.md#deleterepositorypipelinevariable) | **Delete** /repositories/{username}/{repo_slug}/pipelines_config/variables/{variable_uuid} | 
[**GetPipelineForRepository**](PipelinesApi.md#getpipelineforrepository) | **Get** /repositories/{username}/{repo_slug}/pipelines/{pipeline_uuid} | 
[**GetPipelineStepForRepository**](PipelinesApi.md#getpipelinestepforrepository) | **Get** /repositories/{username}/{repo_slug}/pipelines/{pipeline_uuid}/steps/{step_uuid} | 
[**GetPipelineStepLogForRepository**](PipelinesApi.md#getpipelinesteplogforrepository) | **Get** /repositories/{username}/{repo_slug}/pipelines/{pipeline_uuid}/steps/{step_uuid}/log | 
[**GetPipelineStepsForRepository**](PipelinesApi.md#getpipelinestepsforrepository) | **Get** /repositories/{username}/{repo_slug}/pipelines/{pipeline_uuid}/steps/ | 
[**GetPipelineVariableForTeam**](PipelinesApi.md#getpipelinevariableforteam) | **Get** /teams/{username}/pipelines_config/variables/{variable_uuid} | 
[**GetPipelineVariableForUser**](PipelinesApi.md#getpipelinevariableforuser) | **Get** /users/{username}/pipelines_config/variables/{variable_uuid} | 
[**GetPipelineVariablesForTeam**](PipelinesApi.md#getpipelinevariablesforteam) | **Get** /teams/{username}/pipelines_config/variables/ | 
[**GetPipelineVariablesForUser**](PipelinesApi.md#getpipelinevariablesforuser) | **Get** /users/{username}/pipelines_config/variables/ | 
[**GetPipelinesForRepository**](PipelinesApi.md#getpipelinesforrepository) | **Get** /repositories/{username}/{repo_slug}/pipelines/ | 
[**GetRepositoryPipelineConfig**](PipelinesApi.md#getrepositorypipelineconfig) | **Get** /repositories/{username}/{repo_slug}/pipelines_config | 
[**GetRepositoryPipelineKnownHost**](PipelinesApi.md#getrepositorypipelineknownhost) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/ssh/known_hosts/{known_host_uuid} | 
[**GetRepositoryPipelineKnownHosts**](PipelinesApi.md#getrepositorypipelineknownhosts) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/ssh/known_hosts/ | 
[**GetRepositoryPipelineSchedule**](PipelinesApi.md#getrepositorypipelineschedule) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/schedules/{schedule_uuid} | 
[**GetRepositoryPipelineScheduleExecutions**](PipelinesApi.md#getrepositorypipelinescheduleexecutions) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/schedules/{schedule_uuid}/executions/ | 
[**GetRepositoryPipelineSchedules**](PipelinesApi.md#getrepositorypipelineschedules) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/schedules/ | 
[**GetRepositoryPipelineSshKeyPair**](PipelinesApi.md#getrepositorypipelinesshkeypair) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/ssh/key_pair | 
[**GetRepositoryPipelineVariable**](PipelinesApi.md#getrepositorypipelinevariable) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/variables/{variable_uuid} | 
[**GetRepositoryPipelineVariables**](PipelinesApi.md#getrepositorypipelinevariables) | **Get** /repositories/{username}/{repo_slug}/pipelines_config/variables/ | 
[**StopPipeline**](PipelinesApi.md#stoppipeline) | **Post** /repositories/{username}/{repo_slug}/pipelines/{pipeline_uuid}/stopPipeline | 
[**UpdatePipelineVariableForTeam**](PipelinesApi.md#updatepipelinevariableforteam) | **Put** /teams/{username}/pipelines_config/variables/{variable_uuid} | 
[**UpdatePipelineVariableForUser**](PipelinesApi.md#updatepipelinevariableforuser) | **Put** /users/{username}/pipelines_config/variables/{variable_uuid} | 
[**UpdateRepositoryPipelineConfig**](PipelinesApi.md#updaterepositorypipelineconfig) | **Put** /repositories/{username}/{repo_slug}/pipelines_config | 
[**UpdateRepositoryPipelineKeyPair**](PipelinesApi.md#updaterepositorypipelinekeypair) | **Put** /repositories/{username}/{repo_slug}/pipelines_config/ssh/key_pair | 
[**UpdateRepositoryPipelineKnownHost**](PipelinesApi.md#updaterepositorypipelineknownhost) | **Put** /repositories/{username}/{repo_slug}/pipelines_config/ssh/known_hosts/{known_host_uuid} | 
[**UpdateRepositoryPipelineVariable**](PipelinesApi.md#updaterepositorypipelinevariable) | **Put** /repositories/{username}/{repo_slug}/pipelines_config/variables/{variable_uuid} | 


<a name="createpipelineforrepository"></a>
# **CreatePipelineForRepository**
> Pipeline CreatePipelineForRepository (string username, string repo_slug, Pipeline _body)



Endpoint to create and initiate a pipeline.  There are a couple of different options to initiate a pipeline, where the payload of the request will determine which type of pipeline will be instantiated. # Trigger a Pipeline for a branch or tag One way to trigger pipelines is by specifying the reference for which you want to trigger a pipeline (e.g. a branch or tag).  The specified reference will be used to determine which pipeline definition from the `bitbucket-pipelines.yml` file will be applied to initiate the pipeline. The pipeline will then do a clone of the repository and checkout the latest revision of the specified reference.  ### Example  ``` $ curl -X POST -is -u username:password \\   -H 'Content-Type: application/json' \\  https://api.bitbucket.org/2.0/repositories/jeroendr/meat-demo2/pipelines/ \\   -d '   {     \"target\": {       \"ref_type\": \"branch\",        \"type\": \"pipeline_ref_target\",        \"ref_name\": \"master\"     }   }' ``` # Trigger a Pipeline for a commit on a branch or tag You can initiate a pipeline for a specific commit and in the context of a specified reference (e.g. a branch, tag or bookmark). The specified reference will be used to determine which pipeline definition from the bitbucket-pipelines.yml file will be applied to initiate the pipeline. The pipeline will clone the repository and then do a checkout the specified reference.   The following reference types are supported:  * `branch`  * `named_branch` * `bookmark`   * `tag`  ### Example  ``` $ curl -X POST -is -u username:password \\   -H 'Content-Type: application/json' \\   https://api.bitbucket.org/2.0/repositories/jeroendr/meat-demo2/pipelines/ \\   -d '   {     \"target\": {       \"commit\": {         \"type\": \"commit\",          \"hash\": \"ce5b7431602f7cbba007062eeb55225c6e18e956\"       },        \"ref_type\": \"branch\",        \"type\": \"pipeline_ref_target\",        \"ref_name\": \"master\"     }   }' ``` # Trigger a specific pipeline definition for a commit You can trigger a specific pipeline that is defined in your `bitbucket-pipelines.yml` file for a specific commit.  In addition to the commit revision, you specify the type and pattern of the selector that identifies the pipeline definition. The resulting pipeline will then clone the repository and checkout the specified revision.  ### Example  ``` $ curl -X POST -is -u username:password \\   -H 'Content-Type: application/json' \\  https://api.bitbucket.org/2.0/repositories/jeroendr/meat-demo2/pipelines/ \\  -d '   {      \"target\": {       \"commit\": {          \"hash\":\"a3c4e02c9a3755eccdc3764e6ea13facdf30f923\",          \"type\":\"commit\"        },         \"selector\": {            \"type\":\"custom\",               \"pattern\":\"Deploy to production\"           },         \"type\":\"pipeline_commit_target\"    }   }' ``` # Trigger a specific pipeline definition for a commit on a branch or tag You can trigger a specific pipeline that is defined in your `bitbucket-pipelines.yml` file for a specific commit in the context of a specified reference.  In addition to the commit revision, you specify the type and pattern of the selector that identifies the pipeline definition, as well as the reference information. The resulting pipeline will then clone the repository a checkout the specified reference.  ### Example  ``` $ curl -X POST -is -u username:password \\   -H 'Content-Type: application/json' \\  https://api.bitbucket.org/2.0/repositories/jeroendr/meat-demo2/pipelines/ \\  -d '   {      \"target\": {       \"commit\": {          \"hash\":\"a3c4e02c9a3755eccdc3764e6ea13facdf30f923\",          \"type\":\"commit\"        },        \"selector\": {           \"type\": \"custom\",           \"pattern\": \"Deploy to production\"        },        \"type\": \"pipeline_ref_target\",        \"ref_name\": \"master\",        \"ref_type\": \"branch\"      }   }' ``` 

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreatePipelineForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var _body = new Pipeline(); // Pipeline | The pipeline to initiate.

            try
            {
                Pipeline result = apiInstance.CreatePipelineForRepository(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreatePipelineForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **_body** | [**Pipeline**](Pipeline.md)| The pipeline to initiate. | 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpipelinevariableforteam"></a>
# **CreatePipelineVariableForTeam**
> PipelineVariable CreatePipelineVariableForTeam (string username, PipelineVariable _body = null)



Create an account level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreatePipelineVariableForTeamExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var _body = new PipelineVariable(); // PipelineVariable | The variable to create. (optional) 

            try
            {
                PipelineVariable result = apiInstance.CreatePipelineVariableForTeam(username, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreatePipelineVariableForTeam: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The variable to create. | [optional] 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpipelinevariableforuser"></a>
# **CreatePipelineVariableForUser**
> PipelineVariable CreatePipelineVariableForUser (string username, PipelineVariable _body = null)



Create a user level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreatePipelineVariableForUserExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var _body = new PipelineVariable(); // PipelineVariable | The variable to create. (optional) 

            try
            {
                PipelineVariable result = apiInstance.CreatePipelineVariableForUser(username, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreatePipelineVariableForUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The variable to create. | [optional] 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createrepositorypipelineknownhost"></a>
# **CreateRepositoryPipelineKnownHost**
> PipelineKnownHost CreateRepositoryPipelineKnownHost (string username, string repo_slug, PipelineKnownHost _body)



Create a repository level known host.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreateRepositoryPipelineKnownHostExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var _body = new PipelineKnownHost(); // PipelineKnownHost | The known host to create.

            try
            {
                PipelineKnownHost result = apiInstance.CreateRepositoryPipelineKnownHost(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreateRepositoryPipelineKnownHost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **_body** | [**PipelineKnownHost**](PipelineKnownHost.md)| The known host to create. | 

### Return type

[**PipelineKnownHost**](PipelineKnownHost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createrepositorypipelineschedule"></a>
# **CreateRepositoryPipelineSchedule**
> PipelineSchedule CreateRepositoryPipelineSchedule (string username, string repo_slug)



Create a schedule for the given repository.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreateRepositoryPipelineScheduleExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PipelineSchedule result = apiInstance.CreateRepositoryPipelineSchedule(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreateRepositoryPipelineSchedule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PipelineSchedule**](PipelineSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createrepositorypipelinevariable"></a>
# **CreateRepositoryPipelineVariable**
> PipelineVariable CreateRepositoryPipelineVariable (string username, string repo_slug, PipelineVariable _body)



Create a repository level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class CreateRepositoryPipelineVariableExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var _body = new PipelineVariable(); // PipelineVariable | The variable to create.

            try
            {
                PipelineVariable result = apiInstance.CreateRepositoryPipelineVariable(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.CreateRepositoryPipelineVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The variable to create. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletepipelinevariableforteam"></a>
# **DeletePipelineVariableForTeam**
> void DeletePipelineVariableForTeam (string username, string variable_uuid)



Delete a team level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeletePipelineVariableForTeamExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to delete.

            try
            {
                apiInstance.DeletePipelineVariableForTeam(username, variable_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeletePipelineVariableForTeam: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable to delete. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletepipelinevariableforuser"></a>
# **DeletePipelineVariableForUser**
> void DeletePipelineVariableForUser (string username, string variable_uuid)



Delete an account level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeletePipelineVariableForUserExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to delete.

            try
            {
                apiInstance.DeletePipelineVariableForUser(username, variable_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeletePipelineVariableForUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable to delete. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleterepositorypipelinekeypair"></a>
# **DeleteRepositoryPipelineKeyPair**
> void DeleteRepositoryPipelineKeyPair (string username, string repo_slug)



Delete the repository SSH key pair.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeleteRepositoryPipelineKeyPairExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                apiInstance.DeleteRepositoryPipelineKeyPair(username, repo_slug);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeleteRepositoryPipelineKeyPair: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleterepositorypipelineknownhost"></a>
# **DeleteRepositoryPipelineKnownHost**
> void DeleteRepositoryPipelineKnownHost (string username, string repo_slug, string known_host_uuid)



Delete a repository level known host.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeleteRepositoryPipelineKnownHostExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var known_host_uuid = known_host_uuid_example;  // string | The UUID of the known host to delete.

            try
            {
                apiInstance.DeleteRepositoryPipelineKnownHost(username, repo_slug, known_host_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeleteRepositoryPipelineKnownHost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **known_host_uuid** | **string**| The UUID of the known host to delete. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleterepositorypipelineschedule"></a>
# **DeleteRepositoryPipelineSchedule**
> void DeleteRepositoryPipelineSchedule (string username, string repo_slug, string schedule_uuid)



Delete a schedule.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeleteRepositoryPipelineScheduleExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var schedule_uuid = schedule_uuid_example;  // string | The uuid of the schedule.

            try
            {
                apiInstance.DeleteRepositoryPipelineSchedule(username, repo_slug, schedule_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeleteRepositoryPipelineSchedule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **schedule_uuid** | **string**| The uuid of the schedule. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleterepositorypipelinevariable"></a>
# **DeleteRepositoryPipelineVariable**
> void DeleteRepositoryPipelineVariable (string username, string repo_slug, string variable_uuid)



Delete a repository level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class DeleteRepositoryPipelineVariableExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to delete.

            try
            {
                apiInstance.DeleteRepositoryPipelineVariable(username, repo_slug, variable_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.DeleteRepositoryPipelineVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **variable_uuid** | **string**| The UUID of the variable to delete. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelineforrepository"></a>
# **GetPipelineForRepository**
> Pipeline GetPipelineForRepository (string username, string repo_slug, string pipeline_uuid)



Retrieve a specified pipeline

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var pipeline_uuid = pipeline_uuid_example;  // string | The pipeline UUID.

            try
            {
                Pipeline result = apiInstance.GetPipelineForRepository(username, repo_slug, pipeline_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **pipeline_uuid** | **string**| The pipeline UUID. | 

### Return type

[**Pipeline**](Pipeline.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinestepforrepository"></a>
# **GetPipelineStepForRepository**
> PipelineStep GetPipelineStepForRepository (string username, string repo_slug, string pipeline_uuid, string step_uuid)



Retrieve a given step of a pipeline.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineStepForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var pipeline_uuid = pipeline_uuid_example;  // string | The UUID of the pipeline.
            var step_uuid = step_uuid_example;  // string | The UUID of the step.

            try
            {
                PipelineStep result = apiInstance.GetPipelineStepForRepository(username, repo_slug, pipeline_uuid, step_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineStepForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **pipeline_uuid** | **string**| The UUID of the pipeline. | 
 **step_uuid** | **string**| The UUID of the step. | 

### Return type

[**PipelineStep**](PipelineStep.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinesteplogforrepository"></a>
# **GetPipelineStepLogForRepository**
> void GetPipelineStepLogForRepository (string username, string repo_slug, string pipeline_uuid, string step_uuid)



Retrieve the log file for a given step of a pipeline.  This endpoint supports (and encourages!) the use of [HTTP Range requests](https://tools.ietf.org/html/rfc7233) to deal with potentially very large log files.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineStepLogForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var pipeline_uuid = pipeline_uuid_example;  // string | The UUID of the pipeline.
            var step_uuid = step_uuid_example;  // string | The UUID of the step.

            try
            {
                apiInstance.GetPipelineStepLogForRepository(username, repo_slug, pipeline_uuid, step_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineStepLogForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **pipeline_uuid** | **string**| The UUID of the pipeline. | 
 **step_uuid** | **string**| The UUID of the step. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinestepsforrepository"></a>
# **GetPipelineStepsForRepository**
> PaginatedPipelineSteps GetPipelineStepsForRepository (string username, string repo_slug, string pipeline_uuid)



Find steps for the given pipeline.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineStepsForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var pipeline_uuid = pipeline_uuid_example;  // string | The UUID of the pipeline.

            try
            {
                PaginatedPipelineSteps result = apiInstance.GetPipelineStepsForRepository(username, repo_slug, pipeline_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineStepsForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **pipeline_uuid** | **string**| The UUID of the pipeline. | 

### Return type

[**PaginatedPipelineSteps**](PaginatedPipelineSteps.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinevariableforteam"></a>
# **GetPipelineVariableForTeam**
> PipelineVariable GetPipelineVariableForTeam (string username, string variable_uuid)



Retrieve a team level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineVariableForTeamExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to retrieve.

            try
            {
                PipelineVariable result = apiInstance.GetPipelineVariableForTeam(username, variable_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineVariableForTeam: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable to retrieve. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinevariableforuser"></a>
# **GetPipelineVariableForUser**
> PipelineVariable GetPipelineVariableForUser (string username, string variable_uuid)



Retrieve a user level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineVariableForUserExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to retrieve.

            try
            {
                PipelineVariable result = apiInstance.GetPipelineVariableForUser(username, variable_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineVariableForUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable to retrieve. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinevariablesforteam"></a>
# **GetPipelineVariablesForTeam**
> PaginatedPipelineVariables GetPipelineVariablesForTeam (string username)



Find account level variables.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineVariablesForTeamExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.

            try
            {
                PaginatedPipelineVariables result = apiInstance.GetPipelineVariablesForTeam(username);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineVariablesForTeam: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 

### Return type

[**PaginatedPipelineVariables**](PaginatedPipelineVariables.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinevariablesforuser"></a>
# **GetPipelineVariablesForUser**
> PaginatedPipelineVariables GetPipelineVariablesForUser (string username)



Find user level variables.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelineVariablesForUserExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.

            try
            {
                PaginatedPipelineVariables result = apiInstance.GetPipelineVariablesForUser(username);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelineVariablesForUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 

### Return type

[**PaginatedPipelineVariables**](PaginatedPipelineVariables.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpipelinesforrepository"></a>
# **GetPipelinesForRepository**
> PaginatedPipelines GetPipelinesForRepository (string username, string repo_slug)



Find pipelines

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetPipelinesForRepositoryExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PaginatedPipelines result = apiInstance.GetPipelinesForRepository(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetPipelinesForRepository: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PaginatedPipelines**](PaginatedPipelines.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelineconfig"></a>
# **GetRepositoryPipelineConfig**
> PipelinesConfig GetRepositoryPipelineConfig (string username, string repo_slug)



Retrieve the repository pipelines configuration.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineConfigExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PipelinesConfig result = apiInstance.GetRepositoryPipelineConfig(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineConfig: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PipelinesConfig**](PipelinesConfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelineknownhost"></a>
# **GetRepositoryPipelineKnownHost**
> PipelineKnownHost GetRepositoryPipelineKnownHost (string username, string repo_slug, string known_host_uuid)



Retrieve a repository level known host.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineKnownHostExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var known_host_uuid = known_host_uuid_example;  // string | The UUID of the known host to retrieve.

            try
            {
                PipelineKnownHost result = apiInstance.GetRepositoryPipelineKnownHost(username, repo_slug, known_host_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineKnownHost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **known_host_uuid** | **string**| The UUID of the known host to retrieve. | 

### Return type

[**PipelineKnownHost**](PipelineKnownHost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelineknownhosts"></a>
# **GetRepositoryPipelineKnownHosts**
> PaginatedPipelineKnownHosts GetRepositoryPipelineKnownHosts (string username, string repo_slug)



Find repository level known hosts.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineKnownHostsExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PaginatedPipelineKnownHosts result = apiInstance.GetRepositoryPipelineKnownHosts(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineKnownHosts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PaginatedPipelineKnownHosts**](PaginatedPipelineKnownHosts.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelineschedule"></a>
# **GetRepositoryPipelineSchedule**
> PipelineSchedule GetRepositoryPipelineSchedule (string username, string repo_slug, string schedule_uuid)



Retrieve a schedule by its UUID.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineScheduleExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var schedule_uuid = schedule_uuid_example;  // string | The uuid of the schedule.

            try
            {
                PipelineSchedule result = apiInstance.GetRepositoryPipelineSchedule(username, repo_slug, schedule_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineSchedule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **schedule_uuid** | **string**| The uuid of the schedule. | 

### Return type

[**PipelineSchedule**](PipelineSchedule.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelinescheduleexecutions"></a>
# **GetRepositoryPipelineScheduleExecutions**
> PaginatedPipelineScheduleExecutions GetRepositoryPipelineScheduleExecutions (string username, string repo_slug)



Retrieve the executions of a given schedule.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineScheduleExecutionsExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PaginatedPipelineScheduleExecutions result = apiInstance.GetRepositoryPipelineScheduleExecutions(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineScheduleExecutions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PaginatedPipelineScheduleExecutions**](PaginatedPipelineScheduleExecutions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelineschedules"></a>
# **GetRepositoryPipelineSchedules**
> PaginatedPipelineSchedules GetRepositoryPipelineSchedules (string username, string repo_slug)



Retrieve the configured schedules for the given repository.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineSchedulesExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PaginatedPipelineSchedules result = apiInstance.GetRepositoryPipelineSchedules(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineSchedules: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PaginatedPipelineSchedules**](PaginatedPipelineSchedules.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelinesshkeypair"></a>
# **GetRepositoryPipelineSshKeyPair**
> PipelineSshKeyPair GetRepositoryPipelineSshKeyPair (string username, string repo_slug)



Retrieve the repository SSH key pair excluding the SSH private key. The private key is a write only field and will never be exposed in the logs or the REST API.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineSshKeyPairExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PipelineSshKeyPair result = apiInstance.GetRepositoryPipelineSshKeyPair(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineSshKeyPair: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PipelineSshKeyPair**](PipelineSshKeyPair.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelinevariable"></a>
# **GetRepositoryPipelineVariable**
> PipelineVariable GetRepositoryPipelineVariable (string username, string repo_slug, string variable_uuid)



Retrieve a repository level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineVariableExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to retrieve.

            try
            {
                PipelineVariable result = apiInstance.GetRepositoryPipelineVariable(username, repo_slug, variable_uuid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **variable_uuid** | **string**| The UUID of the variable to retrieve. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getrepositorypipelinevariables"></a>
# **GetRepositoryPipelineVariables**
> PaginatedPipelineVariables GetRepositoryPipelineVariables (string username, string repo_slug)



Find repository level variables.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class GetRepositoryPipelineVariablesExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.

            try
            {
                PaginatedPipelineVariables result = apiInstance.GetRepositoryPipelineVariables(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.GetRepositoryPipelineVariables: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 

### Return type

[**PaginatedPipelineVariables**](PaginatedPipelineVariables.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stoppipeline"></a>
# **StopPipeline**
> void StopPipeline (string username, string repo_slug, string pipeline_uuid)



Signal the stop of a pipeline and all of its steps that not have completed yet.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class StopPipelineExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var pipeline_uuid = pipeline_uuid_example;  // string | The UUID of the pipeline.

            try
            {
                apiInstance.StopPipeline(username, repo_slug, pipeline_uuid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.StopPipeline: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **pipeline_uuid** | **string**| The UUID of the pipeline. | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatepipelinevariableforteam"></a>
# **UpdatePipelineVariableForTeam**
> PipelineVariable UpdatePipelineVariableForTeam (string username, string variable_uuid, PipelineVariable _body)



Update a team level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdatePipelineVariableForTeamExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable.
            var _body = new PipelineVariable(); // PipelineVariable | The updated variable.

            try
            {
                PipelineVariable result = apiInstance.UpdatePipelineVariableForTeam(username, variable_uuid, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdatePipelineVariableForTeam: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The updated variable. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatepipelinevariableforuser"></a>
# **UpdatePipelineVariableForUser**
> PipelineVariable UpdatePipelineVariableForUser (string username, string variable_uuid, PipelineVariable _body)



Update a user level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdatePipelineVariableForUserExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable.
            var _body = new PipelineVariable(); // PipelineVariable | The updated variable.

            try
            {
                PipelineVariable result = apiInstance.UpdatePipelineVariableForUser(username, variable_uuid, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdatePipelineVariableForUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **variable_uuid** | **string**| The UUID of the variable. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The updated variable. | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updaterepositorypipelineconfig"></a>
# **UpdateRepositoryPipelineConfig**
> PipelinesConfig UpdateRepositoryPipelineConfig (string username, string repo_slug, PipelinesConfig _body)



Update the pipelines configuration for a repository.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdateRepositoryPipelineConfigExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var _body = new PipelinesConfig(); // PipelinesConfig | The updated repository pipelines configuration.

            try
            {
                PipelinesConfig result = apiInstance.UpdateRepositoryPipelineConfig(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdateRepositoryPipelineConfig: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **_body** | [**PipelinesConfig**](PipelinesConfig.md)| The updated repository pipelines configuration. | 

### Return type

[**PipelinesConfig**](PipelinesConfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updaterepositorypipelinekeypair"></a>
# **UpdateRepositoryPipelineKeyPair**
> PipelineSshKeyPair UpdateRepositoryPipelineKeyPair (string username, string repo_slug, PipelineSshKeyPair _body)



Create or update the repository SSH key pair. The private key will be set as a default SSH identity in your build container.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdateRepositoryPipelineKeyPairExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var _body = new PipelineSshKeyPair(); // PipelineSshKeyPair | The created or updated SSH key pair.

            try
            {
                PipelineSshKeyPair result = apiInstance.UpdateRepositoryPipelineKeyPair(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdateRepositoryPipelineKeyPair: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **_body** | [**PipelineSshKeyPair**](PipelineSshKeyPair.md)| The created or updated SSH key pair. | 

### Return type

[**PipelineSshKeyPair**](PipelineSshKeyPair.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updaterepositorypipelineknownhost"></a>
# **UpdateRepositoryPipelineKnownHost**
> PipelineKnownHost UpdateRepositoryPipelineKnownHost (string username, string repo_slug, string known_host_uuid, PipelineKnownHost _body)



Update a repository level known host.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdateRepositoryPipelineKnownHostExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var known_host_uuid = known_host_uuid_example;  // string | The UUID of the known host to update.
            var _body = new PipelineKnownHost(); // PipelineKnownHost | The updated known host.

            try
            {
                PipelineKnownHost result = apiInstance.UpdateRepositoryPipelineKnownHost(username, repo_slug, known_host_uuid, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdateRepositoryPipelineKnownHost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **known_host_uuid** | **string**| The UUID of the known host to update. | 
 **_body** | [**PipelineKnownHost**](PipelineKnownHost.md)| The updated known host. | 

### Return type

[**PipelineKnownHost**](PipelineKnownHost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updaterepositorypipelinevariable"></a>
# **UpdateRepositoryPipelineVariable**
> PipelineVariable UpdateRepositoryPipelineVariable (string username, string repo_slug, string variable_uuid, PipelineVariable _body)



Update a repository level variable.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class UpdateRepositoryPipelineVariableExample
    {
        public void main()
        {
            
            var apiInstance = new PipelinesApi();
            var username = username_example;  // string | The account.
            var repo_slug = repo_slug_example;  // string | The repository.
            var variable_uuid = variable_uuid_example;  // string | The UUID of the variable to update.
            var _body = new PipelineVariable(); // PipelineVariable | The updated variable

            try
            {
                PipelineVariable result = apiInstance.UpdateRepositoryPipelineVariable(username, repo_slug, variable_uuid, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PipelinesApi.UpdateRepositoryPipelineVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| The account. | 
 **repo_slug** | **string**| The repository. | 
 **variable_uuid** | **string**| The UUID of the variable to update. | 
 **_body** | [**PipelineVariable**](PipelineVariable.md)| The updated variable | 

### Return type

[**PipelineVariable**](PipelineVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

