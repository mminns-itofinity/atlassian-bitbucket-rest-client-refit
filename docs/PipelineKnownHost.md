# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineKnownHost
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**PublicKey** | [**PipelineSshPublicKey**](PipelineSshPublicKey.md) | The public key of the known host. | [optional] 
**Hostname** | **string** | The hostname of the known host. | [optional] 
**Uuid** | **string** | The UUID identifying the known host. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

