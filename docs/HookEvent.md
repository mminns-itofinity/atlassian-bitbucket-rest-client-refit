# Atlassian.Bitbucket.Rest.Client.Refit.Model.HookEvent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Category** | **string** | The category this event belongs to. | [optional] 
**Label** | **string** | Summary of the webhook event type. | [optional] 
**Description** | **string** | More detailed description of the webhook event type. | [optional] 
**_Event** | **string** | The event identifier. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

