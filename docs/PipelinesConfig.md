# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelinesConfig
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Enabled** | **bool?** | Whether Pipelines is enabled for the repository. | [optional] 
**Repository** | [**Repository**](Repository.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

