# Atlassian.Bitbucket.Rest.Client.Refit.Model.Project
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Description** | **string** |  | [optional] 
**Links** | [**ProjectLinks**](ProjectLinks.md) |  | [optional] 
**Uuid** | **string** | The project&#39;s immutable id. | [optional] 
**CreatedOn** | **DateTime?** |  | [optional] 
**Key** | **string** | The project&#39;s key. | [optional] 
**Owner** | [**Team**](Team.md) |  | [optional] 
**UpdatedOn** | **DateTime?** |  | [optional] 
**IsPrivate** | **bool?** |  Indicates whether the project is publicly accessible, or whether it is private to the team and consequently only visible to team members. Note that private projects cannot contain public repositories. | [optional] 
**Name** | **string** | The name of the project. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

