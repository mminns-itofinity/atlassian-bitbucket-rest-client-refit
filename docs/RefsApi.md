# Atlassian.Bitbucket.Rest.Client.Refit.Api.RefsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugRefsBranchesGet**](RefsApi.md#repositoriesusernamereposlugrefsbranchesget) | **Get** /repositories/{username}/{repo_slug}/refs/branches | 
[**RepositoriesUsernameRepoSlugRefsBranchesNameGet**](RefsApi.md#repositoriesusernamereposlugrefsbranchesnameget) | **Get** /repositories/{username}/{repo_slug}/refs/branches/{name} | 
[**RepositoriesUsernameRepoSlugRefsGet**](RefsApi.md#repositoriesusernamereposlugrefsget) | **Get** /repositories/{username}/{repo_slug}/refs | 
[**RepositoriesUsernameRepoSlugRefsTagsGet**](RefsApi.md#repositoriesusernamereposlugrefstagsget) | **Get** /repositories/{username}/{repo_slug}/refs/tags | 
[**RepositoriesUsernameRepoSlugRefsTagsNameGet**](RefsApi.md#repositoriesusernamereposlugrefstagsnameget) | **Get** /repositories/{username}/{repo_slug}/refs/tags/{name} | 
[**RepositoriesUsernameRepoSlugRefsTagsPost**](RefsApi.md#repositoriesusernamereposlugrefstagspost) | **Post** /repositories/{username}/{repo_slug}/refs/tags | 


<a name="repositoriesusernamereposlugrefsbranchesget"></a>
# **RepositoriesUsernameRepoSlugRefsBranchesGet**
> Error RepositoriesUsernameRepoSlugRefsBranchesGet (string username, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsBranchesGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugRefsBranchesGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsBranchesGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugrefsbranchesnameget"></a>
# **RepositoriesUsernameRepoSlugRefsBranchesNameGet**
> Error RepositoriesUsernameRepoSlugRefsBranchesNameGet (string username, string name, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsBranchesNameGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string | 
            var name = name_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugRefsBranchesNameGet(username, name, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsBranchesNameGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **name** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugrefsget"></a>
# **RepositoriesUsernameRepoSlugRefsGet**
> Error RepositoriesUsernameRepoSlugRefsGet (string username, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugRefsGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugrefstagsget"></a>
# **RepositoriesUsernameRepoSlugRefsTagsGet**
> Error RepositoriesUsernameRepoSlugRefsTagsGet (string username, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsTagsGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string |  The username for the owner of the repository. This can either be the `username` of the owner or the `UUID` of the owner (surrounded by curly-braces (`{}`)). Owners can be users or teams. 
            var repo_slug = repo_slug_example;  // string |  The repo slug for the repository.  This can either be the `repo_slug` of the repository or the `UUID` of the repository (surrounded by curly-braces (`{}`)) 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugRefsTagsGet(username, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsTagsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  The username for the owner of the repository. This can either be the &#x60;username&#x60; of the owner or the &#x60;UUID&#x60; of the owner (surrounded by curly-braces (&#x60;{}&#x60;)). Owners can be users or teams.  | 
 **repo_slug** | **string**|  The repo slug for the repository.  This can either be the &#x60;repo_slug&#x60; of the repository or the &#x60;UUID&#x60; of the repository (surrounded by curly-braces (&#x60;{}&#x60;))  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugrefstagsnameget"></a>
# **RepositoriesUsernameRepoSlugRefsTagsNameGet**
> Error RepositoriesUsernameRepoSlugRefsTagsNameGet (string username, string name, string repo_slug)





### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsTagsNameGetExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string | 
            var name = name_example;  // string | 
            var repo_slug = repo_slug_example;  // string | 

            try
            {
                Error result = apiInstance.RepositoriesUsernameRepoSlugRefsTagsNameGet(username, name, repo_slug);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsTagsNameGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **name** | **string**|  | 
 **repo_slug** | **string**|  | 

### Return type

[**Error**](Error.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="repositoriesusernamereposlugrefstagspost"></a>
# **RepositoriesUsernameRepoSlugRefsTagsPost**
> Tag RepositoriesUsernameRepoSlugRefsTagsPost (string username, string repo_slug, Tag _body)



Creates a new tag in the specified repository.  The payload of the POST should consist of a JSON document that contains the name of the tag and the target hash.  ``` {     \"name\" : \"new tag name\",     \"target\" : {         \"hash\" : \"target commit hash\",     } } ```  This endpoint does support using short hash prefixes for the commit hash, but it may return a 400 response if the provided prefix is ambiguous. Using a full commit hash is the preferred approach.

### Example
```csharp
using System;
using System.Diagnostics;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;

namespace Example
{
    public class RepositoriesUsernameRepoSlugRefsTagsPostExample
    {
        public void main()
        {
            
            // Configure API key authorization: api_key
            Configuration.Default.ApiKey.Add("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.ApiKeyPrefix.Add("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";
            // Configure OAuth2 access token for authorization: oauth2
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RefsApi();
            var username = username_example;  // string |  The username for the owner of the repository. This can either be the `username` of the owner or the `UUID` of the owner (surrounded by curly-braces (`{}`)). Owners can be users or teams. 
            var repo_slug = repo_slug_example;  // string |  The repo slug for the repository.  This can either be the `repo_slug` of the repository or the `UUID` of the repository (surrounded by curly-braces (`{}`)) 
            var _body = new Tag(); // Tag | 

            try
            {
                Tag result = apiInstance.RepositoriesUsernameRepoSlugRefsTagsPost(username, repo_slug, _body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RefsApi.RepositoriesUsernameRepoSlugRefsTagsPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  The username for the owner of the repository. This can either be the &#x60;username&#x60; of the owner or the &#x60;UUID&#x60; of the owner (surrounded by curly-braces (&#x60;{}&#x60;)). Owners can be users or teams.  | 
 **repo_slug** | **string**|  The repo slug for the repository.  This can either be the &#x60;repo_slug&#x60; of the repository or the &#x60;UUID&#x60; of the repository (surrounded by curly-braces (&#x60;{}&#x60;))  | 
 **_body** | [**Tag**](Tag.md)|  | 

### Return type

[**Tag**](Tag.md)

### Authorization

[api_key](../README.md#api_key), [basic](../README.md#basic), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

