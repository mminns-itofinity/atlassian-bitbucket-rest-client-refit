# Atlassian.Bitbucket.Rest.Client.Refit.Model.PullrequestEndpoint
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Commit** | [**PullrequestMergeCommit**](PullrequestMergeCommit.md) |  | [optional] 
**Repository** | [**Repository**](Repository.md) |  | [optional] 
**Branch** | [**PullrequestEndpointBranch**](PullrequestEndpointBranch.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

