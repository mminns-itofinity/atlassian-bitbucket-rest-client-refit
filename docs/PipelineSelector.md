# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineSelector
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The type of selector. | 
**Pattern** | **string** | The name of the matching pipeline definition. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

