# Atlassian.Bitbucket.Rest.Client.Refit.Model.IssueContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Raw** | **string** | The text as it was typed by a user. | [optional] 
**Markup** | **string** | The type of markup language the content is to be interpreted in. | [optional] 
**Html** | **string** | The user&#39;s markup rendered as HTML. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

