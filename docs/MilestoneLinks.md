# Atlassian.Bitbucket.Rest.Client.Refit.Model.MilestoneLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

