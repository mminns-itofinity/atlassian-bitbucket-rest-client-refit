# Atlassian.Bitbucket.Rest.Client.Refit.Model.Group
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Name** | **string** |  | [optional] 
**Links** | [**GroupLinks**](GroupLinks.md) |  | [optional] 
**FullSlug** | **string** | The concatenation of the owner&#39;s username and the group&#39;s slug, separated with a colon (e.g. &#x60;acme:developers&#x60;)  | [optional] 
**Members** | **int?** | The number of members in this group | [optional] 
**Owner** | [**Account**](Account.md) |  | [optional] 
**Slug** | **string** | The \&quot;sluggified\&quot; version of the group&#39;s name. This contains only ASCII characters and can therefore be slightly different than the name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

