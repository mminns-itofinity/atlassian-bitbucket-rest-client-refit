# Atlassian.Bitbucket.Rest.Client.Refit.Model.ErrorError
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | **string** |  | 
**Data** | **Object** | Optional structured data that is endpoint-specific. | [optional] 
**Detail** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

