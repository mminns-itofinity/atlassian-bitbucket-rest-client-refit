# Atlassian.Bitbucket.Rest.Client.Refit.Model.IssueLinks
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attachments** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Self** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Watch** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Comments** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Html** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 
**Vote** | [**SubjectTypesUserEvents**](SubjectTypesUserEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

