# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineCommitTarget
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Commit** | [**Commit**](Commit.md) |  | [optional] 
**Selector** | [**PipelineSelector**](PipelineSelector.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

