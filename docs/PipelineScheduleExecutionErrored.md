# Atlassian.Bitbucket.Rest.Client.Refit.Model.PipelineScheduleExecutionErrored
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Error** | [**PipelineError**](PipelineError.md) | The error that caused the execution to fail. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

