/* 
 * Bitbucket API
 *
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * OpenAPI spec version: 2.0
 * Contact: support@bitbucket.org
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using System.Reflection;

namespace Atlassian.Bitbucket.Rest.Client.Refit.Test
{
    /// <summary>
    ///  Class for testing PaginatedPipelines
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PaginatedPipelinesTests
    {
        // TODO uncomment below to declare an instance variable for PaginatedPipelines
        //private PaginatedPipelines instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PaginatedPipelines
            //instance = new PaginatedPipelines();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PaginatedPipelines
        /// </summary>
        [Test]
        public void PaginatedPipelinesInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PaginatedPipelines
            //Assert.IsInstanceOfType<PaginatedPipelines> (instance, "variable 'instance' is a PaginatedPipelines");
        }

        /// <summary>
        /// Test the property 'Next'
        /// </summary>
        [Test]
        public void NextTest()
        {
            // TODO unit test for the property 'Next'
        }
        /// <summary>
        /// Test the property 'Values'
        /// </summary>
        [Test]
        public void ValuesTest()
        {
            // TODO unit test for the property 'Values'
        }
        /// <summary>
        /// Test the property 'Pagelen'
        /// </summary>
        [Test]
        public void PagelenTest()
        {
            // TODO unit test for the property 'Pagelen'
        }
        /// <summary>
        /// Test the property 'Size'
        /// </summary>
        [Test]
        public void SizeTest()
        {
            // TODO unit test for the property 'Size'
        }
        /// <summary>
        /// Test the property 'Page'
        /// </summary>
        [Test]
        public void PageTest()
        {
            // TODO unit test for the property 'Page'
        }
        /// <summary>
        /// Test the property 'Previous'
        /// </summary>
        [Test]
        public void PreviousTest()
        {
            // TODO unit test for the property 'Previous'
        }

    }

}
