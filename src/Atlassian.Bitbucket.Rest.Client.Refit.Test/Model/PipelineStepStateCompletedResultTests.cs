/* 
 * Bitbucket API
 *
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * OpenAPI spec version: 2.0
 * Contact: support@bitbucket.org
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using System.Reflection;

namespace Atlassian.Bitbucket.Rest.Client.Refit.Test
{
    /// <summary>
    ///  Class for testing PipelineStepStateCompletedResult
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PipelineStepStateCompletedResultTests
    {
        // TODO uncomment below to declare an instance variable for PipelineStepStateCompletedResult
        //private PipelineStepStateCompletedResult instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PipelineStepStateCompletedResult
            //instance = new PipelineStepStateCompletedResult();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PipelineStepStateCompletedResult
        /// </summary>
        [Test]
        public void PipelineStepStateCompletedResultInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PipelineStepStateCompletedResult
            //Assert.IsInstanceOfType<PipelineStepStateCompletedResult> (instance, "variable 'instance' is a PipelineStepStateCompletedResult");
        }

        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Test]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }

    }

}
