/* 
 * Bitbucket API
 *
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * OpenAPI spec version: 2.0
 * Contact: support@bitbucket.org
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using System.Reflection;

namespace Atlassian.Bitbucket.Rest.Client.Refit.Test
{
    /// <summary>
    ///  Class for testing PipelineScheduleExecutionExecuted
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PipelineScheduleExecutionExecutedTests
    {
        // TODO uncomment below to declare an instance variable for PipelineScheduleExecutionExecuted
        //private PipelineScheduleExecutionExecuted instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PipelineScheduleExecutionExecuted
            //instance = new PipelineScheduleExecutionExecuted();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PipelineScheduleExecutionExecuted
        /// </summary>
        [Test]
        public void PipelineScheduleExecutionExecutedInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PipelineScheduleExecutionExecuted
            //Assert.IsInstanceOfType<PipelineScheduleExecutionExecuted> (instance, "variable 'instance' is a PipelineScheduleExecutionExecuted");
        }

        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Test]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }
        /// <summary>
        /// Test the property 'Pipeline'
        /// </summary>
        [Test]
        public void PipelineTest()
        {
            // TODO unit test for the property 'Pipeline'
        }

    }

}
