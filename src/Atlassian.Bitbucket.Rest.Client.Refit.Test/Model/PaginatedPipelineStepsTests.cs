/* 
 * Bitbucket API
 *
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * OpenAPI spec version: 2.0
 * Contact: support@bitbucket.org
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Atlassian.Bitbucket.Rest.Client.Refit.Api;
using Atlassian.Bitbucket.Rest.Client.Refit.Model;
using Atlassian.Bitbucket.Rest.Client.Refit.Client;
using System.Reflection;

namespace Atlassian.Bitbucket.Rest.Client.Refit.Test
{
    /// <summary>
    ///  Class for testing PaginatedPipelineSteps
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PaginatedPipelineStepsTests
    {
        // TODO uncomment below to declare an instance variable for PaginatedPipelineSteps
        //private PaginatedPipelineSteps instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PaginatedPipelineSteps
            //instance = new PaginatedPipelineSteps();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PaginatedPipelineSteps
        /// </summary>
        [Test]
        public void PaginatedPipelineStepsInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PaginatedPipelineSteps
            //Assert.IsInstanceOfType<PaginatedPipelineSteps> (instance, "variable 'instance' is a PaginatedPipelineSteps");
        }

        /// <summary>
        /// Test the property 'Next'
        /// </summary>
        [Test]
        public void NextTest()
        {
            // TODO unit test for the property 'Next'
        }
        /// <summary>
        /// Test the property 'Values'
        /// </summary>
        [Test]
        public void ValuesTest()
        {
            // TODO unit test for the property 'Values'
        }
        /// <summary>
        /// Test the property 'Pagelen'
        /// </summary>
        [Test]
        public void PagelenTest()
        {
            // TODO unit test for the property 'Pagelen'
        }
        /// <summary>
        /// Test the property 'Size'
        /// </summary>
        [Test]
        public void SizeTest()
        {
            // TODO unit test for the property 'Size'
        }
        /// <summary>
        /// Test the property 'Page'
        /// </summary>
        [Test]
        public void PageTest()
        {
            // TODO unit test for the property 'Page'
        }
        /// <summary>
        /// Test the property 'Previous'
        /// </summary>
        [Test]
        public void PreviousTest()
        {
            // TODO unit test for the property 'Previous'
        }

    }

}
